# Variables

Variable is the basic unit of storage. It consists of 

- an `identifier` (variable name)
- data type 
- initializer (optional)

```java
// e.g.
int x;  // declare variable x
int y = 10;  // declare and initialize variable y with value 10
byte b = 22, a, c = 12; // declare (a, b, c) and initialize b & c.
```

In the above example, we have declare and initialized various variables. 

In ```int x;``` identifier is `x` and `int` is the data type, similarly in `int y = 10;` identifier is `y` , `int` is the data type and variable `y` is initialized with value `10`, what that means is that it stores value `10`. 

## Naming Rules & Conventions

The Naming concentions for various types of variables in Java  are as follows:

- **Case Sensitive**: Variable names are case-sensitive. 
- **Unlimited Length**: They can be an unlimited-length sequence of Unicode letters and  digits.
- **Should begin with**: A `unicode letter`, the dollar sign "`$`", or the underscore character "`_`". 
  - *Although **`$` and `_` are allowed** to start the variable name, **convension** dictates that you **should not do so.***
- **Subsequent characters**:  They may be unicode letters, digits, dollar signs, or  underscore characters. 
- **Conventions/Good Practice**. For Variable name try to use full words instead of abbreviations as it makes it easier to identify, read and understood.
- **Reserved keywords**: Reserved keywords cannot be used as variable name. 
- **oneword Variable**: For single word variable, it should be in lower case 
- **multiWord Variable**: For Multi Word variable, first word will be in lower case and first letter of all the subsequent words should be in uppercase such as `primeMinister`, `primeNumber`, `officeOfHeadMaster`
- **Constant Variable**: All constants variables names should be in uppercase. If more than one word is present in the name, then they should be seperated by `_`  such as `PI`, `ROOT_CAUSE` etc. (*As per convention, the underscore character is never used elsewhere*).
- **ClassName Variable**: Class name should have first letter of every word in variable name in upper case such as `WelcomeHome`, `MainClass` etc.	

- Examples of valid identifiers

  $$TODO$$

- Examples of invalid identifiers

  $$TODO$$

## Scope of Variables

Variables are valid in their own scopes and cannot be used outside of their respective scopes as shown in the example below. The scope is defined by curly brakets `{ }`. Any variable declared with-in the scope cannot be viewed from directly viewed outside its scope as shown in the below example.

- Variable from inner code block cannot be accessed by outside code blocks *and*
- Variables from outer code block can be accessed in inner code block

  ```java
  /* 
   * Copyright (C) 2019 mayank.johri
   *
   * This program is free software: you can redistribute it and/or modify
   * it under the terms of the GNU General Public License as published by
   * the Free Software Foundation, either version 3 of the License, or
   * (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   * GNU General Public License for more details.
   *
   * You should have received a copy of the GNU General Public License
   * along with this program.  If not, see <http://www.gnu.org/licenses/>.
   */
  package scope01;

  /**
   *
   * @author mayank.johri
   */
  public class Scope01 {

      /**
       * @param args the command line arguments
       */
      public static void main(String[] args) {
          // TODO code application logic here
          int x = 10;
          for (int i = 0; i < 3; i++) {
              int y = 100;
              System.out.println("Y: " + y + ", x: " + x + ", i: " + i);
          }
          // both y & i are out of scope here and will result in error if used. 
          // System.out.println("Y: " + y);
          // System.out.println("i: " + i);
          System.out.println("x: " + x);
      }
  }
  ```

- Variable name defined in outer code block can’t be declared again in inner code block as shown in below example

  ```java
  package scope01;
  
  /**
   *
   * @author mayank.johri
   */
  public class Scope02 {
      public static void main(String[] args) {
          // TODO code application logic here
          int x = 10;
          for (int i = 0; i < 3; i++) {
            // uncomment it to observe the error message
  //            int x = 20;
              int y = 100;
              System.out.println("Y: " + y + ", x: " + x + ", i: " + i);
          }
          System.out.println("x: " + x);
      }
  }
  ```

- Variables data can be 

$$TODO$$



- 

## Keywords

| `abstract` | `assert`       | `boolean`  | `break`      |
| ---------- | -------------- | ---------- | ------------ |
| `byte`     | `case`         | `catch`    | `char`       |
| `class`    | `const`        | `continue` | `default`    |
| `do`       | `double`       | `else`     | `enum`       |
| `extends`  | `final`        | `finally`  | `float`      |
| `for`      | `goto`         | `if`       | `implements` |
| `import`   | `instanceof`   | `int`      | `interface`  |
| `long`     | `native`       | `new`      | `package`    |
| `private`  | `protected`    | `public`   | `return`     |
| `short`    | `static`       | `strictfp` | `super`      |
| `switch`   | `synchronized` | `this`     | `throw`      |
| `throws`   | `transient`    | `try`      | `void`       |
| `volatile` | `while`        | **`true`** | **`false`**  |
| **`null`** |                |            |              |

**true**, **false** and **null** are not reserved words but cannot be used as identifiers, because it is literals of built-in types. 
# Introduction: Lets Learn Automation Testing using Java (Part 1): **Core Java**

In this section, we are going to cover basic topics of Core Java, just enough so that you are able to work as an automation engineer, but not enough to become a full fledged Java Developer.

Before we begin our journey for learning the basics of Core Java, we need to install OpenJDK.

## Installing OpenJDK

Oracle's OpenJDK JDK binaries for Windows, macOS, and Linux are available on release-specific pages of `jdk.java.net` as `.tar.gz` or `.zip` archives.

As an example, the archives for `JDK 13` may be found on `jdk.java.net/13` and may be extracted on the command line using

```bash
// for *inx OSes 
$ tar xvf openjdk-13*_bin.tar.gz
// or for Windows
$ unzip openjdk-13*_bin.zip
```

Add `java/bin` folder to PATH environment variable.

## IDE's which one to select

### Eclipse

**URL**: https://www.eclipse.org/downloads/packages/, 

Select “[Eclipse IDE for Java Developers](https://www.eclipse.org/downloads/packages/release/2019-09/r/eclipse-ide-java-developers)” to download for your respective OS.

#### Set JDK in preference
#### Create a workbench
#### Create a project 

### IntelliJ IDEA

**URL**: https://www.jetbrains.com/idea/download/

### Apache NetBeans

**URL**: https://netbeans.apache.org/download/index.html

### Other IDE'S & Notable Editors

- [BlueJ](https://www.bluej.org/)
- [DrJava](http://www.drjava.org/)
- [(Oracle) JDeveloper](https://www.oracle.com/technetwork/developer-tools/jdev/downloads/index.html)
- [Sublime Text](https://www.sublimetext.com/)
- [Atom](https://atom.io/)

Full List of Java IDE'es can be found at https://en.wikipedia.org/wiki/Comparison_of_integrated_development_environments#Java

## Basis Code Description

Below is the basic `java` code, please go through it and then we will discuss about it.

```java
package org.mj.namaskar;

/*
 * This is basic example, to show various components of Basic Java Example
 */
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


class Namaskar {
    static Logger logger = Logger.getLogger("Namaskar");  
    static FileHandler fh;  	
    public static void main(String[] args) {
        // This block configure the logger with handler and formatter  
        try {
			fh = new FileHandler("Namaskar.log");
		} catch (SecurityException | IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, e.getMessage(), e);
		}  
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);
		logger.info("Welcome");
	}
}
```

### Running the Java Code. 

#### Through IDE



#### Through Command line

- **Step 1:** Compile the code 

  ```bash
  $:> javac MyCode.java
  ```

  This will create a `MyCode.class` file

- **Step 2**: Run the compiled file

  ```bash
  java MyCode
  ```

  

### Comments

In java we can use either `//` or `/* */` for comments. Usually for single line comments we use `//` and for multi line comments we use `/* <comment text> */`. 

When we use `//`, in the same line any text after that is treated as comments and for `/* */` all text between `/*` and `*/` are treated as comments irrespective of number of lines between them. 


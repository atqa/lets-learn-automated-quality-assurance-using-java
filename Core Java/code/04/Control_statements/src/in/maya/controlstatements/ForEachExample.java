package in.maya.controlstatements;

public class ForEachExample {

	public static void main(String[] args) {
		int vals[] = {1, 2, 3, 4};
		
		for(int x : vals) {
			System.out.println("x " + x);
		}
	}
}

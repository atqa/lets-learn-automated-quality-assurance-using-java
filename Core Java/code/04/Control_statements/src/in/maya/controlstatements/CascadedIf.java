package in.maya.controlstatements;

public class CascadedIf {

	public static void main(String[] args) {
		int temprature = 10;
		if(temprature < 0) {
			System.out.println("Its freezing lets stay at home");
		}else if(temprature >= 0 && temprature < 10) {
			System.out.println("Its cold");
		}else if (temprature >= 10 && temprature < 25) {
			System.out.println("Its good weather lets go out");
		}else System.out.println("Its too hot, lets stay at home");
	}
}

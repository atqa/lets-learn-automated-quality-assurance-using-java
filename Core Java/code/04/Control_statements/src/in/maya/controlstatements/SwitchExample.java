package in.maya.controlstatements;

public class SwitchExample {

	public static void main(String[] args) {
		
		// Basic Example
		int diceVal = 1;
		switch(diceVal) {
			case 0:
				System.out.println("Got Zero");
				break;
			case 1:
				System.out.println("Got One");
				break;
		}
		
		// Default Example
		diceVal = 4;
		switch(diceVal) {
			case 0:
				System.out.println("Got Zero");
				break;
			case 1:
				System.out.println("Got One");
				break;
			default:
				System.out.println("Sorry Try again");
		}	
		
		// Default Example
		diceVal = 5;
		switch(diceVal) {
			case 0:
			case 2:
			case 4:
				System.out.println("Got even number");
				break;
			case 1:
			case 3:
			case 5:
				System.out.println("Got Odd number");
				break;
			default:
				System.out.println("Sorry Try again");
		}	
		
		// String Example
		String diceText = "three";
		switch(diceText) {
			case "zero":
			case "two":
				System.out.println("Got even number");
				break;
			case "one":
			case "three":
				System.out.println("Got Odd number");
				break;
			default:
				System.out.println("Sorry Try again");
		}	
	}
}

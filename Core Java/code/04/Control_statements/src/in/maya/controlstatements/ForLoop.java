package in.maya.controlstatements;

public class ForLoop {

	public static void main(String[] args) {
		// inline variable declaration
		for(int i=4; i>=0; i--) {
			System.out.println(i);
		}
//		System.out.println(i);
		// Variable declared before hand
		// Use if we need the value of data afterwards
		int data;
		for(data=4; data<=7; data++) {
			System.out.println(data);
		}
		// Gotcha
		// Variable declared before hand
//		int data;
		for(data=4; data<=7; data++) {
			System.out.println(">" + data);
			data = 10;
		}
		// Gotcha 2
		int i=20;
		for(data=2; data<i; data++) {
			System.out.println("="+ data);
			i = 0;
		}
		
		// for loop with multiple variables
		int a, b;
		for (a=10, b=2; a>b ; a--, b++) {
			System.out.println("a" + a + " b "+ b);
		}
		
		// for loop with funny condition
		boolean flg = true;
		for(a = 0; flg; a++ ) {
			if (a == 4){
				flg = false;
			}
			System.out.println("A "+ a);
		}
		
		// for with only condition
		// not normally used. 
		a = 0;
		flg = true;
		for(;flg;) {
			if (a == 4){
				flg = false;
			}
			System.out.println("B "+ a++);
		}
		
		// infinite forloop
		// But why you want to use it, use while instead
		for(;;) {
			System.out.println("C: "+ a++);
			break;
		}
		
	}

}

package in.maya.controlstatements;

public class WhileExample {

	public static void main(String[] args) {
		
		// While can have condition where its code block
		// never gets executed.
		int val = 20;
		
		// Basic Example
		while(val < 25) {
			System.out.println(val++);
		}
		
		val = 20;
		
		// Basic Example
		while(val++ < 25) {
			System.out.println("val: " + val);
		}
		// using flags
		boolean flg = true;
		while (flg) {
			if(val++ > 30) flg = false;
		}
		System.out.println("val  " + val);
	}

}

package in.maya.controlstatements;

public class BreakExamples {

	public static void main(String[] args) {
		// Only single iteration is skipped and remaining
		// iterations will still run. 
		int x = 0;
		while(x++ < 10) {
			if(x%2 == 0) continue;
			System.out.println(x);
		}
	}
}

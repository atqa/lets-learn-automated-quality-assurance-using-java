package in.maya.controlstatements;

public class ControlStatementsIf {

	public static void main(String[] args) {
		int x = 10;
		
		if (x == 10) System.out.println("Welcome");
		else System.out.println("Bye");
		if (x > 10) System.out.println("Welcome 1");
		else System.out.println("Bye 1");
		
		if (x > 10) 
			System.out.println("Welcome 2");
		else 
			System.out.println("Bye 2");
		
		if(x == 10) {
			// Multi line statements
			System.out.println("Welcome 3");
		} else {
			// multi line statements
			System.out.println("Bye 3");
		}
		
		// Nested ifs 
//		if <condition_true>
//			if condition_2
//				if condition_3
		if (x >= 10) {
			// some task 
			// TODO
			if (x % 2 == 0) {
				System.out.println("Good, the number is >= 10 and is even number");
				
			}
		}
		// Cascading of if {Ladder of if-else-if}
//		if (x< 10){
//			
//		}else if (x > 3){
//			
//		}
		int temp = 20;
		if (temp < 0) {
			System.out.println("Its freezing, lets stay home");
		}else if (temp <= 10)  {
			System.out.println();
		}
	}

}

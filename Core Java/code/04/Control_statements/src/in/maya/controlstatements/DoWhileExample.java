package in.maya.controlstatements;

import java.io.IOException;

public class DoWhileExample {

	public static void main(String[] args) throws IOException {
		// In do-while, at least once the code block will run
		int val = 100;
		do {
			System.out.println(++val);
		}while(val < 12);
	
		// Keep asking input from user till proper input is provided. 
		char passwd = 0;
		do {
			System.out.println("Please enter the password:");
			passwd = (char) System.in.read();

		}while(passwd != 'a');
	}
}

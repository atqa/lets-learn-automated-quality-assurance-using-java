package in.maya.controlstatements;

public class NestedIf {

	public static void main(String[] args) {
		int x = 10;
		// Nested ifs 
//		if <condition_true>
//			if condition_2
//				if condition_3
		if (x >= 10) {
			// some task to do 
			if (x % 2 == 0) {
				System.out.println("Good, the number is >= 10 and is even number");
				
			}
		}

	}

}

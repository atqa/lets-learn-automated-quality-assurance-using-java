package in.maya.controlstatements;

public class ReturnExample {

	public static void main(String[] args) {
		// Return returns the control to the caller.
		int val = 10;
		
		while(val++ < 15) {
			if(val == 12) {
				return;
			}
			System.out.println("val" + val);
		}
		System.out.println("Outside the while loop");

	}

}

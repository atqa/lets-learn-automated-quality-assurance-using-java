package org.mj.namaskar;

/*
 * This is basic example, to show various components of Basic Java Example
 */
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


class Namaskar {
    static Logger logger = Logger.getLogger("Namaskar");  
    static FileHandler fh;  	
    public static void main(String[] args) {
        // This block configure the logger with handler and formatter  
        try {
			fh = new FileHandler("Namaskar.log");
		} catch (SecurityException | IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE, e.getMessage(), e);
		}  
        logger.addHandler(fh);
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);
		logger.info("Welcome");
	}
}

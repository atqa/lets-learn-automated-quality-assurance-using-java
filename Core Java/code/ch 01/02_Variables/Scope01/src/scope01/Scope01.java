/* 
 * Copyright (C) 2019 mayank.johri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package scope01;

/**
 *
 * @author mayank.johri
 */
public class Scope01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int x = 10;
        for (int i = 0; i < 3; i++) {
            int y = 100;
            System.out.println("Y: " + y + ", x: " + x + ", i: " + i);
        }
        // both y & i are out of scope here and will result in error if used. 
//         System.out.println("Y: " + y);
//         System.out.println("i: " + i);
        System.out.println("x: " + x);
    }
}

/*
 * Copyright (C) 2019 mayank.johri
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package scope01;

/**
 *
 * @author mayank.johri
 */
public class VarOne {
    public static void main(String[] args) {
        int x=0;
        int X=10;
        // Uncomment the below for the issue with trying to use uninitialzed 
//        variable
//        int x;
        System.out.println(x);
         System.out.println(X);
    }
}

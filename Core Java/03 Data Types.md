# Data Types

Java has two types of datatype

- Primitive Data types / Basic Data Type
- Non-Primitive Data types / Complex Data Type

## Primitive Data type

### Integers

- int 
- long
- bytes
- short

#### `int`

 It is the most commonly used interger and is signed 32 bit type with value in range of -214,7483,648 (-2<sup>31</sup>) to 2,147,483,647 (2<sup>31</sup>-1).

#### `long`

It is used when larger numbers are needed as its a signed 64 bit data type and minimum value of -2<sup>63</sup> and a maximum value of 2<sup>63</sup>-1.

#### `bytes`

It holds very small value as its a signed 8 bit data type and has a range of -128 to 127. They are usually used for data streams and raw binary data.

#### `short`

It is signed 16 bit data type and has a range of -32,768 to 32,767.

**Example**

```java
/**
 * 
 */
package in.co.integer;

/**
 * @author mayank.johri
 *
 */
public class Integer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x = 1000;
		long circleArea = 2147483630;
		byte data = 100;
		short sh_data = 32000;
		System.out.println(x + " - " + circleArea  + " - " +  data  + " - " + sh_data);
	}
}
```

#### Integer Literals

`0b` & `0x` literals can be used to denote, boolean & hex numbers respectively as shown in the below example. 

```java
package in.co.integer;

public class IntegerLiterals {

	public static void main(String[] args) {
		int hexVal = 0x2c;
		int binVal = 0b101100;
		
		System.out.println(hexVal + " : " + binVal);
	}
}
```

#### Using Underscore Characters in Numeric Literals

```java
package in.co.integer;

public class IntegerLiterals {

	public static void main(String[] args) {
		int hexVal = 0x2c;
		int binVal = 0b101100;
		
		System.out.println(hexVal + " : " + binVal);
		// Using Underscore Characters in Numeric Literals
		int x = 10_00;
		long circleArea = 2_147_483_630;
		byte data = 1_0_0;
		short shData = 32_000;
		System.out.println(x + " - " + circleArea  + " - " +  data  + " - " + shData);
	}
}
```

**Rules to Apply**	

- It can be applied only between digits 
- Can have multiple inderscores between two digits.
- It cannot be placed 
  - at the end of Literal/Number
  - adjacent (both before and after) to a decimal points
  - Along with ‘x’, ‘o’, `L` or `F`

#### Floating Point numbers

- float 
- double

#### `float`

It is a single precision 32 bit data type, and is normally used for floating point numbes where high degree of precision is not required. It is not adviceable to use for high precision data. 

#### `double`

It is called double precision 64 bit data type, and is normally used for floating point numbers where high degree of precision is requird. 

All **Transcendental maths functions** such as (sin(), cos(), sqrt() etc) return `double` value.       

Example:

```java

```

#### Characters

The `char` data type is a single 16-bit Unicode character. It has a minimum value of `'\u0000'` (or 0) and a maximum value of `'\uffff'` (or 65,535 inclusive).

#### Boolean

The `boolean` data type has only two possible values: `true` and `false`. Use this data type for simple flags that track true/false conditions.  This data type represents one bit of information, but its "size" isn't  something that's precisely defined.

## Complex Data Type

### Arrays

An *array* is a container object that holds a fixed number of  values of a single type. The length of an array is established when the  array is created. After creation, its length is fixed. 

Each item in an array is called an *element*, and each element is accessed by its numerical *index*. As shown in the below, numbering begins with 0. The  8<sup>th</sup> element.

![imgs/ch03/string.png](imgs/ch03/string.png)

```java

```

